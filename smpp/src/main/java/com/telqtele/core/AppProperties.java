package com.telqtele.core;

import java.io.IOException;
import java.util.Properties;

public class AppProperties {

    private static Properties properties;

    private static void loadPropertiesIfNecessary() {
        if (properties == null) {
            try {
                properties = new Properties();
                properties.loadFromXML(AppProperties.class.getResourceAsStream("/properties.xml"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String getStringValue(String key, String fallback) {
        loadPropertiesIfNecessary();
        return properties.getProperty(key, fallback);
    }

    public static int getIntValue(String key, int fallback) {
        loadPropertiesIfNecessary();
        return Integer.valueOf(properties.getProperty(key, Integer.toString(fallback)));
    }

    public static boolean getBooleanValue(String key, boolean fallback) {
        loadPropertiesIfNecessary();
        return Boolean.valueOf(properties.getProperty(key, Boolean.toString(fallback)));
    }

    public static byte getByteValue(String key, byte fallback) {
        loadPropertiesIfNecessary();
        return Byte.valueOf(properties.getProperty(key, Byte.toString(fallback)));
    }
}
