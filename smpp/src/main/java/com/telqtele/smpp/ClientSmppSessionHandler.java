package com.telqtele.smpp;

import com.cloudhopper.smpp.impl.DefaultSmppSessionHandler;
import com.cloudhopper.smpp.pdu.PduRequest;
import com.cloudhopper.smpp.pdu.PduResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClientSmppSessionHandler extends DefaultSmppSessionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ClientSmppSessionHandler.class);


    public ClientSmppSessionHandler() {
        super(logger);
    }

    @Override
    public void firePduRequestExpired(PduRequest pduRequest) {
        logger.warn("PDU request expired: {}", pduRequest);
    }

    @Override
    public PduResponse firePduRequestReceived(PduRequest pduRequest) {
        PduResponse response = pduRequest.createResponse();

        return response;
    }

}
