package com.telqtele.smpp;

import com.cloudhopper.smpp.impl.DefaultSmppClient;
import com.cloudhopper.smpp.pdu.EnquireLink;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.pdu.SubmitSmResp;
import com.cloudhopper.smpp.type.*;
import com.telqtele.core.AppProperties;
import com.telqtele.smpp.connection.ConnectionHandler;
import com.telqtele.smpp.model.Connection;
import com.telqtele.smpp.model.Server;
import com.telqtele.smpp.model.Sms;
import com.telqtele.smpp.service.ConfigurationService;
import com.telqtele.smpp.service.ConnectionService;
import com.telqtele.smpp.service.ServerService;
import com.telqtele.smpp.service.SmsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class TelqClient {
    private static final Logger logger = LoggerFactory.getLogger(TelqClient.class);
    private static Map<String, Server> serverMap;

    private static DefaultSmppClient smppClient;

    private static int loadServerTime;
    private static int loadSmsTime;
    private static int enquireLinkTime;
    private static int loadConnectionsTime;

    private static long lastServerLoad;
    private static long lastSmsLoad;
    private static long lastEnquireLink;
    private static long lastConnectionLoad;

    private static boolean shouldBeRunning = true;

    public static void main(String [] args) {
        serverMap = new HashMap<>();
        init();
    }

    private static void init() {
        loadTimes();
        ServerService serverService = new ServerService();
        ConnectionService connectionService = new ConnectionService();
        SmsService smsService = new SmsService();
        createDefaultSmppClientIfNecessary();
        while (shouldBeRunning) {
            serverHandling(serverService);
            connectionHandling(connectionService);
            enquireLinkHandling();
            smsHandling(smsService);
        }
    }

    private static void serverHandling(ServerService serverService) {
        if (serverMustBeLoaded()) {
            registerServerIfAbsent(serverService.loadServer());
            lastServerLoad = System.currentTimeMillis();
        }
    }

    private static void connectionHandling(ConnectionService connectionService) {
        if (connectionsMustBeLoaded()) {
            connectionService.loadConnections().forEach(connection -> {
                if (connection.isOpen()) {
                    establishNewConnectionForServerIfAbsent(connection);
                } else {
                    closeConnection(connection);
                }
            });
            lastConnectionLoad = System.currentTimeMillis();
        }
    }

    private static void enquireLinkHandling() {
        if (linksMustBeEnquired()) {
            serverMap.forEach((k, server) ->
                server.getConnectionHandlerMap().entrySet().stream().filter(entry -> entry.getValue().isRunning()).collect(Collectors.toList())
                    .forEach(stringConnectionHandlerEntry -> {
                        try {
                            stringConnectionHandlerEntry.getValue().getSmppSession().enquireLink(new EnquireLink(), 10000);
                        } catch (RecoverablePduException | UnrecoverablePduException | SmppTimeoutException | SmppChannelException | InterruptedException e) {
                            e.printStackTrace();
                        }
                    })
            );
            lastEnquireLink = System.currentTimeMillis();
        }
    }

    private static void smsHandling(SmsService smsService) {
        if (smsMustBeLoaded()) {
            List<Sms> smsList = smsService.loadSms();
            smsList
                .stream()
                .filter(Sms::isNotDispatched)
                .collect(Collectors.toList())
                .forEach(sms -> {
                    try {
                        SubmitSm submitSm = smsService.createSubmitSm(sms);
                        SubmitSmResp submitSmResp =
                            serverMap.get(sms.getServerName())
                                .getConnectionHandlerMap()
                                .get(sms.getConnectionName())
                                .getSmppSession()
                                .submit(submitSm, 10000);
                        System.out.println("SubmitSmResp: " + submitSmResp.getResultMessage());
                        if (submitSmResp.getResultMessage().equals("OK")) {
                            sms.setDispatched(true);
                        }
                    } catch (InterruptedException | RecoverablePduException | SmppChannelException | SmppTimeoutException | UnrecoverablePduException e) {
                        e.printStackTrace();
                    }
                });
            smsService.saveSms(smsList);
            lastSmsLoad = System.currentTimeMillis();
        }
    }

    private static void closeConnection(Connection connection) {
        Server server = serverMap.get(connection.getServerName());
        server.getConnectionHandlerMap().get(connection.getName()).closeConnection();
    }

    private static boolean serverMustBeLoaded() {
        return (System.currentTimeMillis() - lastServerLoad) > loadServerTime;
    }

    private static boolean smsMustBeLoaded() {
        return (System.currentTimeMillis() - lastSmsLoad) > loadSmsTime;
    }

    private static boolean linksMustBeEnquired() {
        return (System.currentTimeMillis() - lastEnquireLink) > enquireLinkTime;
    }

    private static boolean connectionsMustBeLoaded() {
        return (System.currentTimeMillis() - lastConnectionLoad) > loadConnectionsTime;
    }

    private static void registerServerIfAbsent(Server server) {
        serverMap.computeIfAbsent(server.getName(), s -> {
            server.setConnectionHandlerMap(new HashMap<>());
            server.setSmppSessionConfiguration(new ConfigurationService().getConfig(server));
            return server;
        });
    }

    private static void establishNewConnectionForServerIfAbsent(Connection connection) {
        Server server = serverMap.get(connection.getServerName());
        server.getConnectionHandlerMap()
            .computeIfAbsent(
                connection.getName(), s ->
                    establishNewConnection(server));
    }

    private static ConnectionHandler establishNewConnection(Server server) {
        ConnectionHandler connectionHandler = new ConnectionHandler(smppClient, server.getSmppSessionConfiguration());
        connectionHandler.init();
        return connectionHandler;
    }

    private static void createDefaultSmppClientIfNecessary() {
        if (smppClient == null) {
            smppClient = new DefaultSmppClient();
            logger.info("Created new DefaultSmppClient");
        }
    }

    private static void loadTimes() {
        loadServerTime = AppProperties.getIntValue("loadServerTime", 30000);
        loadSmsTime = AppProperties.getIntValue("loadSmsTime", 10000);
        enquireLinkTime = AppProperties.getIntValue("enquireLinkTime", 10000);
        loadConnectionsTime = AppProperties.getIntValue("loadConnectionsTime", 300000);

        lastServerLoad = System.currentTimeMillis() - loadServerTime;
        lastSmsLoad = System.currentTimeMillis() - loadSmsTime;
        lastEnquireLink = System.currentTimeMillis() - enquireLinkTime;
        lastConnectionLoad = System.currentTimeMillis() - loadConnectionsTime;
    }
}


