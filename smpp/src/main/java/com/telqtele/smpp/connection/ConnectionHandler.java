package com.telqtele.smpp.connection;

import com.cloudhopper.smpp.SmppClient;
import com.cloudhopper.smpp.SmppSession;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.cloudhopper.smpp.type.SmppChannelException;
import com.cloudhopper.smpp.type.SmppTimeoutException;
import com.cloudhopper.smpp.type.UnrecoverablePduException;
import com.telqtele.smpp.ClientSmppSessionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionHandler {
    private static final Logger logger = LoggerFactory.getLogger(ConnectionHandler.class);

    private final ClientSmppSessionHandler clientSmppSessionHandler;
    private boolean isRunning;
    private final SmppClient smppClient;
    private SmppSession smppSession;
    private final SmppSessionConfiguration smppSessionConfiguration;

    public ConnectionHandler(
            final SmppClient smppClient,
            final SmppSessionConfiguration smppSessionConfiguration) {
        this.clientSmppSessionHandler = new ClientSmppSessionHandler();
        isRunning = false;
        this.smppClient = smppClient;
        this.smppSessionConfiguration = smppSessionConfiguration;
    }

    public void init() {
        try {
            smppSession = bind();
            isRunning = true;
            logger.info("new bind for Server");
        } catch (UnrecoverablePduException | SmppChannelException | InterruptedException | SmppTimeoutException e) {
            e.printStackTrace();
            logger.info("Could not bind");
        }
    }

    private SmppSession bind() throws UnrecoverablePduException, SmppChannelException, InterruptedException, SmppTimeoutException {
        return smppClient.bind(smppSessionConfiguration, clientSmppSessionHandler);
    }

    public void closeConnection() {
        smppSession.unbind(10000);
        smppSession.close();
        smppSession.destroy();
        isRunning = false;
    }

    public boolean isRunning() {
        return isRunning;
    }

    public SmppSession getSmppSession() {
        return smppSession;
    }
}
