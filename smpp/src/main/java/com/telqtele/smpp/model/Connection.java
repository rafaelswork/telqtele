package com.telqtele.smpp.model;

public class Connection {
    private String name;
    private String serverName;
    private boolean open;

    public String getName() {
        return name;
    }

    public String getServerName() {
        return serverName;
    }

    public boolean isOpen() {
        return open;
    }
}
