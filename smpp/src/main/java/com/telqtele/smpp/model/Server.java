package com.telqtele.smpp.model;

import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.telqtele.smpp.connection.ConnectionHandler;

import java.util.Map;

public class Server {
    private String name;
    private String host;
    private int port;
    private String systemId;
    private String password;
    private Map<String, ConnectionHandler> connectionHandlerMap;
    private SmppSessionConfiguration smppSessionConfiguration;

    public Server(String name, String host, int port, String systemId, String password) {
        this.name = name;
        this.host = host;
        this.port = port;
        this.systemId = systemId;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getSystemId() {
        return systemId;
    }

    public String getPassword() {
        return password;
    }

    public Map<String, ConnectionHandler> getConnectionHandlerMap() {
        return connectionHandlerMap;
    }

    public SmppSessionConfiguration getSmppSessionConfiguration() {
        return smppSessionConfiguration;
    }

    public void setSmppSessionConfiguration(SmppSessionConfiguration smppSessionConfiguration) {
        this.smppSessionConfiguration = smppSessionConfiguration;
    }

    public void setConnectionHandlerMap(Map<String, ConnectionHandler> connectionHandlerMap) {
        this.connectionHandlerMap = connectionHandlerMap;
    }
}
