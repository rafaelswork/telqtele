package com.telqtele.smpp.model;

public class Sms {
    private String destinationTo;
    private String destinationFrom;
    private String message;
    private boolean dispatched;
    private String connectionName;
    private String serverName;

    public Sms(String destinationTo,
               String destinationFrom,
               String message) {
        this.destinationTo = destinationTo;
        this.destinationFrom = destinationFrom;
        this.message = message;
    }

    public String getDestinationTo() {
        return destinationTo;
    }

    public String getDestinationFrom() {
        return destinationFrom;
    }

    public String getMessage() {
        return message;
    }

    public boolean isNotDispatched() {
        return !dispatched;
    }

    public void setDispatched(boolean dispatched) {
        this.dispatched = dispatched;
    }

    public String getConnectionName() {
        return connectionName;
    }

    public String getServerName() {
        return serverName;
    }
}
