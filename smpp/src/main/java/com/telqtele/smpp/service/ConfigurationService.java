package com.telqtele.smpp.service;

import com.cloudhopper.smpp.SmppBindType;
import com.cloudhopper.smpp.SmppConstants;
import com.cloudhopper.smpp.SmppSessionConfiguration;
import com.telqtele.core.AppProperties;
import com.telqtele.smpp.model.Server;

public class ConfigurationService {

    public SmppSessionConfiguration getConfig(Server server) {
        SmppSessionConfiguration configuration = new SmppSessionConfiguration();
        configuration.setName(server.getName());
        configuration.setHost(server.getHost());
        configuration.setPort(server.getPort());
        configuration.setSystemId(server.getSystemId());
        configuration.setPassword(server.getPassword());
        configuration.setType(SmppBindType.TRANSCEIVER);
        configuration.setWindowSize(AppProperties.getIntValue("smppConfigurationWindowSize", 1));
        configuration.setConnectTimeout(AppProperties.getIntValue("smppConfigurationConnectTimeout", 10000));
        configuration.getLoggingOptions().setLogBytes(AppProperties.getBooleanValue("smppConfigurationLoggingOptions", true));
        configuration.setRequestExpiryTimeout(AppProperties.getIntValue("smppConfigurationRequestExpiryTimeout", 30000));
        configuration.setWindowMonitorInterval(AppProperties.getIntValue("smppConfigurationWindowMonitorInterval", 15000));
        configuration.setCountersEnabled(AppProperties.getBooleanValue("smppConfigurationCountersEnabled", true));
        configuration.setInterfaceVersion(AppProperties.getByteValue("smppConfigurationInterfaceVersion", SmppConstants.VERSION_5_0));
        return configuration;
    }
}
