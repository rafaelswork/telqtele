package com.telqtele.smpp.service;

import com.google.gson.Gson;
import com.telqtele.smpp.model.Connection;

import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

public class ConnectionService {

    public List<Connection> loadConnections() {
        // now we load testdata from json. normally we would load testdata from db or any other place
        return Arrays.asList(new Gson().fromJson(
                new InputStreamReader(Connection.class.getResourceAsStream("/testdata/connection.json")), Connection[].class));
    }
}
