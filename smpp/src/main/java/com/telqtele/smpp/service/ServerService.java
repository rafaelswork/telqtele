package com.telqtele.smpp.service;

import com.google.gson.Gson;
import com.telqtele.smpp.model.Server;

import java.io.InputStreamReader;

public class ServerService {

    public Server loadServer() {
        // now we load testdata from json. normally we would load testdata from db or any other place
        return new Gson().fromJson(
                new InputStreamReader(Server.class.getResourceAsStream("/testdata/server.json")),Server.class);
    }
}
