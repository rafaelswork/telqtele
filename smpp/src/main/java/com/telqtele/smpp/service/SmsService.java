package com.telqtele.smpp.service;

import com.cloudhopper.commons.charset.CharsetUtil;
import com.cloudhopper.smpp.pdu.SubmitSm;
import com.cloudhopper.smpp.type.Address;
import com.cloudhopper.smpp.type.SmppInvalidArgumentException;
import com.google.gson.Gson;
import com.telqtele.smpp.model.Sms;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public class SmsService {

    private Address createAddress(String destination) {
        return new Address((byte) 0x03, (byte) 0x00, destination);
    }

    private byte[] createMessageAsBytes(String message) {
        return CharsetUtil.encode(message, CharsetUtil.CHARSET_GSM);
    }

    public SubmitSm createSubmitSm(Sms sms) throws SmppInvalidArgumentException {
        SubmitSm submitSm = new SubmitSm();
        submitSm.setShortMessage(createMessageAsBytes(sms.getMessage()));
        submitSm.setDestAddress(createAddress(sms.getDestinationTo()));
        submitSm.setSourceAddress(createAddress(sms.getDestinationFrom()));
        return submitSm;
    }

    public List<Sms> loadSms() {
        // now we load testdata from json. normally we would load testdata from db or any other place
        return Arrays.asList(new Gson().fromJson(
            new InputStreamReader(Sms.class.getResourceAsStream("/testdata/sms.json")), Sms[].class));
    }

    public void saveSms(List<Sms> sms) {
        try {
            URL resourceUrl = getClass().getResource("/testdata/sms.json");
            File file = new File(resourceUrl.toURI());
            OutputStream stream = new FileOutputStream(file);
            OutputStreamWriter writer = new OutputStreamWriter(stream);
            writer.append(new Gson().toJson(sms.toArray()));
            writer.close();
            stream.close();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }
    }
}
